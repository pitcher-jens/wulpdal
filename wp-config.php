<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wulpdal');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3iYE_#FuSzng_Ox!toq-uq5 )l__Pd75@enWX}J=c4RN]NwJA`uvd>do7Bkwp29U');
define('SECURE_AUTH_KEY',  ':KU%QxfLbnXI9k7^e!1q2$8a>jY*JwEl!}#K0&.ULwHhRRkWz*|Yt~$IZw8#7L V');
define('LOGGED_IN_KEY',    'i!XMH]2{71-m6- GU%_K>]EX#oXOUQj{).[aJud(R=+NRT~lVsLpJu<x-8C~fRc`');
define('NONCE_KEY',        '|C9X.,Kl e,aVszk!+Z<{$cY6f2~%>aM}tG8cV;D5ar|D!;T-6nly4h}T{,N*=4o');
define('AUTH_SALT',        'pR,Z>&M9J9 -Q~py3E[<T-u{G<:wB]UqSnSLH;!k*O^RR@(=Nol#N%W6=(BCOr}U');
define('SECURE_AUTH_SALT', 'FRe9h_ pxy,vaq|%6QgU=9NLQsftBx<G$ rH3Ty`vXqy| B2=EEi=u,~QC;v>w0x');
define('LOGGED_IN_SALT',   'PSHC.5zHYMx6em~sxqC4KpcfwkfD8F^;[h<f6fvTje2_0uB{<HR]ckvU&I?#n/uU');
define('NONCE_SALT',       'k*;_<a>m4_2ily_c,[b?|P57ad`_FIVv.8-Oh%n<aKyz4{,)ib W*+r,w6N^[,Sc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
