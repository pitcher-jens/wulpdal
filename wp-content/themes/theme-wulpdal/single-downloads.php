<?php get_header();

if (have_posts()) :
while (have_posts()) : 
the_post(); 

$title = get_the_title();
$tekst = get_field('tekst');
$download = $_GET["d"];
$file = get_field('bestand');
?>

<main id="home">
    
    <?php if ($download == 1) { ?>
    
    <section>
        <div class="container">
            <div class="container">
            <div class="col-12 col-lg-10 offset-lg-1 no-padding">
                <div class="wrap">
                    <div class="center">
                        <div class="divider"></div>
                        <div class="title">
                            <h2>Download</h2>
                            <br>
                            <?php echo $title; ?>
                            <br><br>
                            <br><br>
                            Uw download is gereed:
                            <br><br>
                            <a href="<?php echo $file; ?>" download class="btn-primary btn" target="blank_">DOWNLOAD</a>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
                             
        </div>
    </section>
    
    <?php } else { ?>
    
    <section>
        <div class="container">
            <div class="container">
            <div class="col-12 col-lg-10 offset-lg-1 no-padding">
                <div class="wrap">
                    <div class="center">
                        <div class="divider"></div>
                        <div class="title">
                            <h2>Download</h2>
                            <br>
                            <?php echo $title; ?>
                            <br><br>
                            Vul uw gegevens in om dit bestand te downloaden.
                        </div>
                        
                    </div>
                </div>
            </div>
                
            <div id="contact">
                <div class="col-12 col-lg-8 offset-lg-2 no-padding">

                    <div class="row">
                            <div class="col">
                                <?php echo do_shortcode( '[contact-form-7 id="436" title="Download"]' ); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <?php } ?>

</main>
    
<?php
endwhile;
endif;
get_footer(); ?>