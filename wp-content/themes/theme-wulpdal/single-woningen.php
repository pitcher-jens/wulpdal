<?php get_header();

if (have_posts()) :
while (have_posts()) : 
the_post(); 

$title = get_the_title();
$omschrijving_titel = get_field('titel');
$tekst = get_field('omschrijving');
$omschrijving = get_field('omschrijving');
$image = get_field('afbeelding');
$curid = get_the_ID();

$post_object = get_field('brochure');
if( $post_object ): 
    // override $post
    $post = $post_object;
    setup_postdata( $post ); 
    $brochure = get_permalink();
    wp_reset_postdata();
endif;
?>

<main id="woningen">
    <header class="small-header center">
        <h1 class="white"><?php echo $title; ?></h1>
    </header>
    <section id="woning-details" class="grey-bg" data-animation="fade-in-up" data-hook=".7">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="row info">
                        <div class="col-12 col-lg-6 image-wrapper">
                            <?php if( !empty($image) ): ?>
                                <div class="image" style="background-image: url('<?php echo $image['url']; ?>')"></div>
                            <?php endif; ?>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="wrap">
                                <div class="title">
                                    <h3><?php echo $omschrijving_titel; ?></h3>
                                </div>
                                <div class="content">
                                    <?php echo $omschrijving; ?>
                                    <div class="button-wrapper">
                                        <a href="<?php echo $brochure; ?>" class="btn download"><i class="wd-icon wd-brochure x2"></i><span>Download de brochure</span></a>
                                        <a href="<?php echo home_url(); ?>/afpsraak-maken?type=Ik%20heb%20interesse%20in%20<?php echo $title; ?>" class="btn btn-primary">Maak een afspraak</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                    $posts = get_field('inventaris');
                    if( $posts ):
                    ?>
                    <div class="row specs">
                        <div class="col-12 col-lg-3">
                            <h3>Inventaris</h3>
                        </div>
                        <div class="col-12 col-lg-7">
                            <?php 
                            echo    '<ul>';
                            foreach( $posts as $post):
                                setup_postdata($post);
                                $invtitle = get_the_title();
                                echo    '<li><i class="wd-icon wd-complete"></i>' . $invtitle . '</li>';
                            endforeach;
                            echo    '</ul>';
                            wp_reset_postdata();
                            ?>
                        </div>

                        <div class="col-12 col-lg-2">
                            <div class="plattegrond btn btn-secondary"><i class="wd-icon wd-plattegrond x3"></i><span>Bekijk plattegrond</span></div>

                        </div>

                    </div>
                            
                    <?php 
                    endif; 
                    ?>
                           
                </div>
            </div>
        </div>
    </section>
    
    <?php get_template_part('builder', 'builder'); ?>
    
    <section id="woning-carousel" class="grey-bg no-top-margin">
        <div class="container">
            
             <div id="woningen-grid" class="row" >
                 <div class="col-12 col-xl-10 offset-xl-1">
                     <div class="row">
                         <div class="col">
                             <div class="divider"></div>
                             <div class="title">
                                 <h2>Bekijk ook deze woningen</h2>
                             </div>
                         </div>
                     </div>
                     <div class="row" data-animation="fade-in-up" data-hook=".7">
                         
                         <?php 
                        //LOAD PRODUCTEN
                        $args=array(
                        'post_type' => 'Woningen',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'post__not_in' => array($curid),
                        'order_by' => 'menu_order',
                        'order' => 'ASC',
                        );
                        $my_query = null;
                        $my_query = new WP_Query($args);
                        if( $my_query->have_posts() ) {
                        while ($my_query->have_posts()) : $my_query->the_post();

                        $title = get_the_title();
                        $tekst = custom_field_excerptII();
                        $actief = get_field('active');
                        $link = get_permalink();
                        ?>
                         
                        <div class="col-12 col-sm-6 col-xl-4">
                            <div class="woning">
                                <a href="<?php echo the_permalink(); ?>">
                                    <div class="image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/wulpdal-woning_de_meeuw.jpg')"></div>
                                    <div class="content center">
                                        <p><?php echo $tekst; ?></p>
                                        <div class="button btn">Bekijk de <?php echo $title; ?></div>
                                    </div>
                                 </a>
                             </div>
                        </div>

                        <?php endwhile;
                        } wp_reset_query();
                        ?>
                         
                     </div>
                     <div class="row">
                          <div class="col center">
                              <a href="<?php echo home_url(); ?>/afpsraak-maken?type=Ik%20heb%20interesse%20in%20<?php echo $title; ?>" class="button btn btn-secondary white">Maak een afspraak</a>
                          </div>
                     </div>
                 </div>
             </div>
             
        </div>
    </section>

</main>

    
<?php
endwhile;
endif;
get_footer(); ?>