<?php
// Custom post 1: Add custom post types

function create_posttype() {
    
    if (get_field('module_products', 'option') == 1) {
        register_post_type( 'Woningen',
            array(
                'labels' => array(
                    'name' => __( 'Woningen' ),
                    'singular_name' => __( 'Woning' ),
                    'add_new' => __( 'Nieuwe woning' ),
                    'add_new_item' => __( 'Nieuwe woning toevoegen' )
                ),
                'menu_icon' => 'dashicons-admin-home',
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'woningen'),
                'supports' => array('title'),
                'taxonomies' => array('topics' ),
                'capability_type'    => 'post',
                'capabilities'       => array( 'create_posts' => true ),       
                'map_meta_cap'       => true,
            )

        );
    }
    
    if (get_field('module_reviews', 'option') == 1) {
        register_post_type( 'Reviews',
            array(
                'labels' => array(
                    'name' => __( 'Reviews' ),
                    'singular_name' => __( 'Review' ),
                    'add_new' => __( 'Nieuw review' ),
                    'add_new_item' => __( 'Nieuwe review toevoegen' )
                ),
                'menu_icon' => 'dashicons-testimonial',
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'reviews'),
                'supports' => array('title'),
                'taxonomies' => array('topics' ),
                'capability_type'    => 'post',
                'capabilities'       => array( 'create_posts' => true ),       
                'map_meta_cap'       => true,
            )

        );
    }
    
    if (get_field('module_team', 'option') == 1) {
        register_post_type( 'Team',
            array(
                'labels' => array(
                    'name' => __( 'Team' ),
                    'singular_name' => __( 'Teamlid' ),
                    'add_new' => __( 'Nieuw teamlid ' ),
                    'add_new_item' => __( 'Nieuwe teamlid toevoegen' )
                ),
                'menu_icon' => 'dashicons-groups',
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'team'),
                'supports' => array('title'),
                'taxonomies' => array('topics' ),
                'capability_type'    => 'post',
                'capabilities'       => array( 'create_posts' => true ),       
                'map_meta_cap'       => true,
            )

        );
    }
    
        register_post_type( 'Nieuws',
            array(
                'labels' => array(
                    'name' => __( 'Nieuws' ),
                    'singular_name' => __( 'Nieuws' ),
                    'add_new' => __( 'Nieuw bericht ' ),
                    'add_new_item' => __( 'Nieuw bericht toevoegen' )
                ),
                'menu_icon' => 'dashicons-format-aside',
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'nieuws'),
                'supports' => array('title'),
                'taxonomies' => array('topics' ),
                'capability_type'    => 'post',
                'capabilities'       => array( 'create_posts' => true ),       
                'map_meta_cap'       => true,
            )

        );
    
        register_post_type( 'Faciliteiten',
            array(
                'labels' => array(
                    'name' => __( 'Faciliteiten' ),
                    'singular_name' => __( 'Faciliteit' ),
                    'add_new' => __( 'Nieuwe faciliteit ' ),
                    'add_new_item' => __( 'Nieuwe faciliteit toevoegen' )
                ),
                'menu_icon' => 'dashicons-star-filled',
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'faciliteiten'),
                'supports' => array('title'),
                'taxonomies' => array('topics' ),
                'capability_type'    => 'post',
                'capabilities'       => array( 'create_posts' => true ),       
                'map_meta_cap'       => true,
            )

        );
    
        register_post_type( 'Downloads',
            array(
                'labels' => array(
                    'name' => __( 'Downloads' ),
                    'singular_name' => __( 'Download' ),
                    'add_new' => __( 'Nieuwe download ' ),
                    'add_new_item' => __( 'Nieuwe download toevoegen' )
                ),
                'menu_icon' => 'dashicons-download',
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'downloads'),
                'supports' => array('title'),
                'taxonomies' => array('topics' ),
                'capability_type'    => 'post',
                'capabilities'       => array( 'create_posts' => true ),       
                'map_meta_cap'       => true,
            )

        );
    
    if (get_field('module_prices', 'option') == 1) {
        register_post_type('pakketten',
            array(
                'labels' => array(
                    'name' => __( 'Pakketten' ),
                    'singular_name' => __( 'Pakketten' ),
                    'add_new' => __( 'Nieuw pakket' ),
                    'add_new_item' => __( 'Nieuw pakket toevoegen' )
                ),
                'menu_icon' => 'dashicons-editor-ul',
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'pakket'),
                'supports' => array('title'),
                'taxonomies' => array('topics' ),
                'capability_type'    => 'post',
                'capabilities'       => array( 'create_posts' => true ),       
                'map_meta_cap'       => true,
            )
        );
    
        register_taxonomy( 'Eigenschappen', 'pakketten',
            array(
                'labels' => array(
                    'name'				=> _x( 'Eigenschappen', 'taxonomy general name', 'cleyo' ),
                    'singular_name'     => _x( 'Eigenschappen', 'taxonomy singular name', 'cleyo' ),
                    'update_item'       => __( 'Werk eigenschap bij', 'cleyo' ),
                    'edit_item'         => __( 'Bewerk eigenschap', 'cleyo' ),
                    'add_new_item'      => __( 'Voeg een nieuwe eigenschap toe', 'cleyo' ),
                    'menu_name' 		=> __( 'Eigenschappen', 'cleyo' ),
                    'new_item_name'     => __( 'Nieuwe eigenschap', 'cleyo' )
                ),
                'hierarchical' => true,
                'query_var' => 'eigenschap',
                'show_admin_column' => false,
            )
        );
    }
        
        register_post_type( 'Inventaris',
            array(
                'labels' => array(
                    'name' => __( 'Inventaris' ),
                    'singular_name' => __( 'Inventaris' ),
                    'add_new' => __( 'Nieuw item' ),
                    'add_new_item' => __( 'Nieuw item toevoegen' )
                ),
                'menu_icon' => 'dashicons-clipboard',
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'inventaris'),
                'supports' => array('title'),
                'taxonomies' => array('topics' ),
                'capability_type'    => 'post',
                'capabilities'       => array( 'create_posts' => true ),       
                'map_meta_cap'       => true,
            )

        );
    
}

// End
?>