<?php

// Config builder 1: Activate options
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

// Hide inactive modules
function my_acf_admin_head() {

    $module_banner = get_field('module_banner', 'option');
    $module_prices = get_field('module_prices', 'option');
    $module_products = get_field('module_products', 'option');
    $module_reviews = get_field('module_reviews', 'option');
    $module_blogs = get_field('module_blogs', 'option');
    $module_home_huidverbetering = get_field('module_home_huidverbetering', 'option');
    $module_tekst = get_field('module_tekst', 'option');
    $module_huidverbetering_slider = get_field('module_huidverbetering_slider', 'option');
    $module_cta = get_field('module_cta', 'option');
    $module_team = get_field('module_team', 'option');
    $module_galerij = get_field('module_galerij', 'option');
    $module_contact = get_field('module_contact', 'option');
    $module_maps = get_field('module_maps', 'option');
    $module_voor_na = get_field('module_voor_na', 'option');
    
    echo    '<style>';
    
    if ($module_banner == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_banner"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_banner"] {';
        echo    'display: none;';
        echo    '}';
    }
    if ($module_prices == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_prices"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_prices"] {';
        echo    'display: none;';
        echo    '}';
    }
    if ($module_products == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_products"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_products"] {';
        echo    'display: none;';
        echo    '}';
    }
    if ($module_reviews == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_reviews"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_reviews"] {';
        echo    'display: none;';
        echo    '}';
    }
    
    if ($module_blogs == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_blogs"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_blogs"] {';
        echo    'display: none;';
        echo    '}';
    }
    
    if ($module_home_huidverbetering == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="home_huidverbetering"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="home_huidverbetering"] {';
        echo    'display: none;';
        echo    '}';
    }
    
    if ($module_tekst == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_tekst"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_tekst"] {';
        echo    'display: none;';
        echo    '}';
    }

    if ($module_huidverbetering_slider == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_huidverbetering_info"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_huidverbetering_info"] {';
        echo    'display: none;';
        echo    '}';
    }
    
    if ($module_team == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_team"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_team"] {';
        echo    'display: none;';
        echo    '}';
    }
    
    if ($module_cta == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_cta"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_cta"] {';
        echo    'display: none;';
        echo    '}';
    }
    
    if ($module_galerij == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_gallerij"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_gallerij"] {';
        echo    'display: none;';
        echo    '}';
    }

    if ($module_contact == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_contact"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_contact"] {';
        echo    'display: none;';
        echo    '}';
    }
    
    if ($module_maps == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="google_maps"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="google_maps"] {';
        echo    'display: none;';
        echo    '}';
    }
    
    if ($module_voor_na == '') {
        echo    '.post-type-page .acf-fc-popup a[data-layout="content_voor_na"] {';
        echo    'display: none;';
        echo    '}';
        echo    '.post-type-woningen .acf-fc-popup a[data-layout="content_voor_na"] {';
        echo    'display: none;';
        echo    '}';
    }
    echo    '</style>';
    
    
    // Display modules when user is Pitcher
    $user_id = get_current_user_id();
    if ($user_id == 1) {
    } else {
        echo    '<style>';
        echo    '#acf-group_5a152e21950ad {display: none;}';
        echo    '</style>';
    }
    
    
     // Weergave
    if( have_rows('admin_weergave', 'option') ): 
        while( have_rows('admin_weergave', 'option') ): the_row(); 
            $super_admin = get_sub_field('super_admin');
            $user = get_sub_field('user');
            if ($user_id == 1) {
                if ($super_admin == '') {
                    add_action( 'admin_menu', 'linked_url' );
                        function linked_url() {
                        add_menu_page( 'linked_url', 'Thema', 'read', '/themes.php', '', 'dashicons-admin-customizer', 100 );
                    }
                    echo    '<style>.toplevel_page_themes .wp-submenu { display: none; }</style>';
                } 
            } else {
                if ($user == '') {
                    add_action( 'admin_menu', 'linked_url' );
                        function linked_url() {
                        add_menu_page( 'linked_url', 'Thema', 'read', '/themes.php', '', 'dashicons-admin-customizer', 100 );
                    }
                    echo    '<style>.toplevel_page_themes .wp-submenu { display: none; }</style>';
                } 
            }
        endwhile;
    endif;
    
}

add_action('acf/input/admin_head', 'my_acf_admin_head');


$user_id = get_current_user_id();
if ($user_id == 1) {
} else {
add_filter('acf/settings/show_admin', '__return_false');
}


?>