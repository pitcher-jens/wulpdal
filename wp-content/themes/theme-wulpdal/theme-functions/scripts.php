<?php
// Load CSS files
function wp_adding_css(){
    wp_enqueue_style( 'main.min', get_template_directory_uri().'/assets/css/main.min.css');
}
add_action( 'wp_enqueue_css', 'wp_adding_css' );

function wp_adding_css_footer(){
    wp_enqueue_style( 'font-awesome.min', get_template_directory_uri().'/assets/css/font-awesome.min.css');
}
add_action( 'wp_enqueue_css', 'wp_adding_css_footer' );

// Load JS files
function mytheme_custom_scripts(){
    wp_enqueue_script( 'library-min', get_template_directory_uri(). '/assets/js/library.min.js' , array(), '1.0',  false );
    wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDynqbE0nucmbxJyYV28MBXMfrir1WKWGg' , array(), '1.0',  false );
    wp_enqueue_script( 'main-min', get_template_directory_uri(). '/assets/js/main.min.js' , array(), '1.0',  false );
    wp_enqueue_script( 'pannellum', get_template_directory_uri(). '/bower_components/pannellum/js/pannellum.js' , array(), '1.0',  false );
    wp_enqueue_script( 'libpannellum', get_template_directory_uri(). '/bower_components/pannellum/js/libpannellum.js' , array(), '1.0',  false );
}
add_action( 'wp_enqueue_scripts', 'mytheme_custom_scripts' );

// Place CSS and move JavaScript code to page footer
add_action('wp_head', 'wp_adding_css', 5);
add_action('wp_footer', 'wp_adding_css_footer', 5);
remove_action('wp_head', 'wp_print_scripts');
remove_action('wp_head', 'wp_print_head_scripts', 9);
remove_action('wp_head', 'wp_enqueue_scripts', 1);
add_action('wp_footer', 'wp_print_scripts', 5);
add_action('wp_footer', 'wp_enqueue_scripts', 5);
add_action('wp_footer', 'wp_print_head_scripts', 5);
?>