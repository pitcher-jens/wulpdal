$(document).ready(function () {
    $('.hamburger').on('click', function () {
        $(this).toggleClass('opened');
        $('nav#main ul').toggleClass('navigation-open');
        $('body').toggleClass('overflow');
    });


    if ($('.light-gallery').length > 0) {
        $(".light-gallery").lightGallery({
            selector: '.grid-item'
        });
    };

    if ($('#panorama').length > 0) {
        pannellum.viewer('panorama', {
            "type": "equirectangular",
            "panorama": "https://pannellum.org/images/cerro-toco-0.jpg",
            "autoLoad": true,
            "mouseZoom": false,
            "showControls": false,
            "compass": false
        });
    }

    if ($('.nieuws-carousel').length > 0) {
        $(this).each(function () {
            $('.nieuws-carousel').owlCarousel({
                loop: false,
                nav: false,
                responsiveClass: true,
                responsive: {
                    0: {
                        margin: 20,
                        items: 1,
                    },
                    600: {
                        items: 2,
                        dots: true,
                        margin: 30,
                    },
                    1000: {
                        items: 3,
                        margin: 40,
                        loop: false,
                        mouseDrag: false
                    }
                }
            });
        })
    }
    if ($('.gallery-carousel').length > 0) {
        $(this).each(function () {
            var owl = $('.gallery-carousel'),
                owlOptions = {
                    loop: false,
                    nav: false,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            margin: 20,
                            items: 1,
                            dots: true
                        },
                        600: {
                            items: 2,
                            dots: true,
                            margin: 20,
                        },
                        1000: {
                            items: 3,
                            margin: 40,
                            loop: false,
                            mouseDrag: false
                        }
                    }
                };

            if ($(window).width() < 991) {
                var owlActive = owl.owlCarousel(owlOptions);
            } else {
                owl.addClass('off');
            }

            $(window).resize(function () {
                if ($(window).width() < 991) {
                    if ($('.owl-carousel').hasClass('off')) {
                        var owlActive = owl.owlCarousel(owlOptions);
                        owl.removeClass('off');
                    }
                } else {
                    if (!$('.owl-carousel').hasClass('off')) {
                        owl.addClass('off').trigger('destroy.owl.carousel');
                        owl.find('.owl-stage-outer').children(':eq(0)').unwrap();
                    }
                }
            });
        })
    }

    if ($('footer .owl-carousel').length > 0) {
        $(this).each(function () {
            $('footer .owl-carousel').owlCarousel({
                loop: false,
                nav: false,
                responsiveClass: true,
                responsive: {
                    0: {
                        margin: 20,
                        items: 1
                    },
                    600: {
                        items: 2,
                        dots: true,
                        margin: 30,
                    },
                    1000: {
                        items: 2,
                        margin: 50,
                        loop: false,
                        mouseDrag: false
                    }
                }
            });
        })
    }


});

if ($('#map').length > 0) {
    $(this).each(function () {
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 12,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(51.534305, 4.071581), // New York

                // How you would like to style the map. 
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [{
                        "hue": "#7fc8ed"
            }, {
                        "saturation": 55
            }, {
                        "lightness": -6
            }, {
                        "visibility": "on"
            }]
        }, {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [{
                        "hue": "#7fc8ed"
            }, {
                        "saturation": 55
            }, {
                        "lightness": -6
            }, {
                        "visibility": "off"
            }]
        }, {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{
                        "hue": "#83cead"
            }, {
                        "saturation": 1
            }, {
                        "lightness": -15
            }, {
                        "visibility": "on"
            }]
        }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "hue": "#f3f4f4"
            }, {
                        "saturation": -84
            }, {
                        "lightness": 59
            }, {
                        "visibility": "on"
            }]
        }, {
                    "featureType": "landscape",
                    "elementType": "labels",
                    "stylers": [{
                        "hue": "#ffffff"
            }, {
                        "saturation": -100
            }, {
                        "lightness": 100
            }, {
                        "visibility": "off"
            }]
        }, {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [{
                        "hue": "#ffffff"
            }, {
                        "saturation": -100
            }, {
                        "lightness": 100
            }, {
                        "visibility": "on"
            }]
        }, {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [{
                        "hue": "#bbbbbb"
            }, {
                        "saturation": -100
            }, {
                        "lightness": 26
            }, {
                        "visibility": "on"
            }]
        }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{
                        "hue": "#ffcc00"
            }, {
                        "saturation": 100
            }, {
                        "lightness": -35
            }, {
                        "visibility": "simplified"
            }]
        }, {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [{
                        "hue": "#ffcc00"
            }, {
                        "saturation": 100
            }, {
                        "lightness": -22
            }, {
                        "visibility": "on"
            }]
        }, {
                    "featureType": "poi.school",
                    "elementType": "all",
                    "stylers": [{
                        "hue": "#d7e4e4"
            }, {
                        "saturation": -60
            }, {
                        "lightness": 23
            }, {
                        "visibility": "on"
            }]
        }]
            };

            // Get the HTML DOM element that will contain your map 
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');
            var mapRoute = document.getElementById('map-route');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);
            var map = new google.maps.Map(mapRoute, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(51.534305, 4.071581),
                map: map,
                title: 'Wulpdal!'
            });
        }

    })
}

var animationController = new ScrollMagic.Controller();
/* FADE IN UP */
if ($('[data-animation="fade-in-up"]').length > 0) {
    $('[data-animation="fade-in-up"]').each(function () {
        var fadeInUp = $(this);
        var revealHook;
        var attr = $(this).attr('data-hook');
        var delay = $(this).attr('data-delay');

        if (typeof attr !== typeof undefined && attr !== false) {
            revealHook = $(this).attr('data-hook');
        } else {
            revealHook = .8
        }
        if (typeof delay !== typeof undefined && delay !== false) {
            delay = $(this).attr('data-delay');
        } else {
            delay = 0
        }
        //Set tween
        var fadeInUpTween = new TimelineMax({
            delay: delay
        });

        fadeInUpTween.staggerFromTo(fadeInUp, .5, {
            alpha: 0,
            y: 50,
            ease: Power1.easeInOut
        }, {
            alpha: 1,
            y: 0,
            ease: Power1.easeInOut
        }, 0.25);

        var scene = new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: revealHook

            })
            .reverse(false)
            .setTween(fadeInUpTween)
            .addTo(animationController)
    });
}
