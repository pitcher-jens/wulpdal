<?php
$afbeelding = get_sub_field('afbeelding');
$logo = get_sub_field('logo');
$formaat = get_sub_field('formaat');
if ($formaat == 'Klein') {
    $class = 'small-header center';
}   
$tekst = get_sub_field('tekst');



echo    '<header class= "' . $class . '" style="background-image: url(' . $afbeelding . ')">';

    if ($logo == 1) {
        echo    '<div class="container-fluid">';
        echo    '<div class="logo">';
        echo    '<img src="' . get_template_directory_uri() . '/assets/images/wulpdal-logo_white.svg" alt="">';
        echo    '</div>';
        echo    '</div>';
    } else if ($tekst == '') { } else {
        echo   '<h1 class="white">' . $tekst . '</h1>';
    }

echo    '</header>';
?>