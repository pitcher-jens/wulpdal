<?php
$titel = get_sub_field('titel');
if( get_sub_field('link') ):
$link = get_sub_field('link');
endif;

echo '<div style="position: relative; float: left; width: 100%; border: solid 1px #000;">';
echo $titel;

if( get_sub_field('link') ):
echo $link[url];
echo $link[title];
endif;
?>

<?php 
//LOAD REVIEWS
$args=array(
'post_type' => 'Reviews',
'post_status' => 'publish',
'posts_per_page' => -1,
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post();
    
    $title = get_the_title();
    $tekst = get_field('tekst');
    $naam = get_field('naam');
    $actief = get_field('active');
    $link = get_permalink();
    
    echo    $title;
    echo    $tekst;
    echo    'Naam: ';
    echo    $naam;
    echo    '<a href="' . $link . '">Lees meer</a>';
    
    endwhile;
} wp_reset_query();
?>

<?php
echo '</div>';
?>