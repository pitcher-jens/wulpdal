<?php
$titel = get_sub_field('titel');

echo '<div style="position: relative; float: left; width: 100%; border: solid 1px #000;">';
echo $titel;
?>

<?php 
//LOAD TEAM
$args=array(
'post_type' => 'Team',
'post_status' => 'publish',
'posts_per_page' => -1,
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post();
    
    $title = get_the_title();
    $tekst = get_field('tekst');
    $afbeelding = get_field('afbeelding');
    $funtie = get_field('functie');
    $actief = get_field('active');
    $link = get_permalink();
    
    echo    $title;
    echo    'Inhoud: ';
    echo    $tekst;
    echo    '<a href="' . $link . '">Lees meer</a>';
    
    endwhile;
} wp_reset_query();
?>

<?php
echo '</div>';
?>