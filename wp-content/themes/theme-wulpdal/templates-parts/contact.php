<?php
$formulier_active = get_sub_field('formulier_active');
$weergave_formulier = get_sub_field('weergave_formulier');
$contact_active = get_sub_field('contact_active');
$knop_active = get_sub_field('knop_active');

if( get_sub_field('knop') ):
$knop = get_sub_field('knop');
endif;
$adres = get_sub_field('adres');
$formulier = get_sub_field('formulier');
?>

<section>
    <div class="container">    
    
        <?php if ($formulier_active == 1) { ?>
        
            <div id="contact">
                 <div class="row">
                
                <?php 
                if ($weergave_formulier == 'Formulier & Google maps') {
                    echo    '<div class="col-12 col-lg-6">';
                } else {
                    echo    '<div class="col-12 col-lg-12">';
                } 
                if( have_rows('formulier') ): 
                    while( have_rows('formulier') ): the_row();   
                    $title = get_sub_field('titel');
                    $subtitel = get_sub_field('subtitel');
                    $formulier = get_sub_field('formulier');
                ?>
                   
                    <div class="row">
                        <div class="col-12">
                            <div class="title">
                                <h2><span><?php echo $title; ?></span></h2>
                                <h3><?php echo $subtitel; ?></h3>
                            </div>
                        </div>
                        <div class="col">
                            <?php echo do_shortcode( $formulier ); ?>
                        </div>
                    </div>
                </div>
        
                <?php if ($weergave_formulier == 'Formulier & Google maps') { ?>
                <div class="col-12 col-lg-6">
                    <div id="map"></div>
                </div>
                <?php } else { ?>
                <div class="hidden-xs hidden-sm hidden-md hidden-lg">
                    <div id="map"></div>
                </div>
                <?php } ?>
                
            </div>
    
        <?php
        endwhile;  
        endif;  
        } ?>
        
        
        <?php if ($contact_active == 1) { 
        if( have_rows('contactgegevens') ): 
            while( have_rows('contactgegevens') ): the_row(); 
            $knop = get_sub_field('knop');
        ?>
        
        <div class="spacing">
            <div id="route" class="no-gutter">
                <div class="row">
                    <div class="col col-12 col-lg-9">
                        <div id="map-route"></div>
                    </div>
                    <div class="col col-12 col-lg-3">
                        <div class="content">
                            <div class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/wulpdal-logo_small_white.svg" alt=""></div>
            
                            <?php 
                            if( have_rows('regels') ): 
                                echo    '<ul>';
                                while( have_rows('regels') ): the_row(); 

                                    $tekst = get_sub_field('regel');
                                    $link = get_sub_field('link');

                                    echo    '<li>';
                                    if ($link == '') { } else {
                                        echo    '<a href="' . $link[url] . '" target="' . $link[target] . '">';
                                    }
                                    echo    $tekst;
                                    if ($link == '') { } else {
                                        echo    '</a>';
                                    }
                                    echo    '</li>';
   
                                endwhile;
                                echo    '</ul>';      
                            endif;  
                            ?>

                            <a href="" class="btn-secondary btn white">Bekijk route</a>
                        </div>
                    </div>
            
                </div>
            </div>
            
        </div>
        
        <?php
        endwhile;  
        endif;  
        } ?>
        
    </div>
</section>

<?php 
if ($knop_active == 1) {
if( get_sub_field('knop') ):
$knop = get_sub_field('knop');
?>

<section class="dark-grey-bg content-overlap-last">
    <div class="container">
        <div class="row center">
            <div class="col"><a href="<?php echo $knop[url]; ?>" class="btn-secondary btn white" target="<?php echo $knop[target]; ?>"><?php echo $knop[title]; ?></a></div>
        </div>
    </div>
</section>

<?php 
endif;
} ?>