<?php
$titel = get_sub_field('titel');
$toon = get_sub_field('toon');
$knop = get_sub_field('knop');
if ($toon == 'Laatste 3 berichten') {
?>

<section class="grey-bg overflow">
    <div class="container">
        <div id="nieuws-grid">
            <div class="row">
                <div class="col center">
                    <div class="divider"></div>
                    <div class="title">
                        <h2><?php echo $titel; ?></h2>
                    </div>
                </div>
            </div>
            <div class="row" data-animation="fade-in-up" data-hook=".7">
                <div class="col">
                    <div class="owl-carousel nieuws-carousel">
                        
                        <?php 
                        //LOAD PRODUCTEN
                        $args=array(
                        'post_type' => 'Nieuws',
                        'post_status' => 'publish',
                        'posts_per_page' => 3,
                        'orderby' => 'date',
                        'order' => 'DESC',
                        );
                        $my_query = null;
                        $my_query = new WP_Query($args);
                        if( $my_query->have_posts() ) {
                            while ($my_query->have_posts()) : $my_query->the_post();

                            $title = get_the_title();
                            $afbeelding = get_field('afbeelding');
                            $tekst = custom_field_excerpt();
                            $link = get_permalink();
                            
                            echo    '<div class="nieuws">';
                            echo    '<a href="' . $link . '">';
                            echo    '<div class="image" style="background-image: url(' . $afbeelding . ')"></div>';
                            echo    '<div class="content center">';
                            echo    '<h4>' . $title . '</h4>';
                            echo    $tekst;
                            echo    '<div class="button btn">Lees bericht</div>';
                            echo    '</div>';
                            echo    '</a>';
                            echo    '</div>';

                            endwhile;
                        } wp_reset_query();
                        ?>                        
                        
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col center">
                    <?php if ($knop == '') {} else { ?>
                    <a href="<?php echo $knop[url]; ?>" class="btn-secondary btn white" target="<?php echo $knop[target]; ?>"><?php echo $knop[title]; ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        
    </div>
</section>

<?php } else if ($toon == 'Alles') { ?>

<section class="grey-bg overflow">
    <div class="container">
        <div id="nieuws-grid">
            <div class="row">
                <div class="col center">
                    <div class="divider"></div>
                    <div class="title">
                        <h2><?php echo $titel; ?></h2>
                    </div>
                </div>
            </div>
            <div class="row" data-animation="fade-in-up" data-hook=".7">
                        
                        <?php 
                        //LOAD PRODUCTEN
                        $args=array(
                        'post_type' => 'Nieuws',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'orderby' => 'date',
                        'order' => 'DESC',
                        );
                        $my_query = null;
                        $my_query = new WP_Query($args);
                        if( $my_query->have_posts() ) {
                            while ($my_query->have_posts()) : $my_query->the_post();

                            $title = get_the_title();
                            $afbeelding = get_field('afbeelding');
                            $tekst = custom_field_excerpt();
                            $link = get_permalink();
                            
                            echo    '<div class="col-md-6 col-lg-4">';
                            echo    '<div class="nieuws">';
                            echo    '<a href="' . $link . '">';
                            echo    '<div class="image" style="background-image: url(' . $afbeelding . ')"></div>';
                            echo    '<div class="content center">';
                            echo    '<h4>' . $title . '</h4>';
                            echo    $tekst;
                            echo    '<div class="button btn">Lees bericht</div>';
                            echo    '</div>';
                            echo    '</a>';
                            echo    '</div>';
                            echo    '</div>';

                            endwhile;
                        } wp_reset_query();
                        ?>                        
                        
            </div>
            
            <div class="row">
                <div class="col center">
                    <?php if ($knop == '') {} else { ?>
                    <a href="<?php echo $knop[url]; ?>" class="btn-secondary btn white" target="<?php echo $knop[target]; ?>"><?php echo $knop[title]; ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        
    </div>
</section>

<?php } ?>