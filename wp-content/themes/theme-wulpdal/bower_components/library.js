/* JQUERY */
// @codekit-prepend "/jquery/dist/jquery.min.js";

/* PANNELLUM */
// @codekit-prepend "/pannellum/js/pannellum.js";
// @codekit-prepend "/pannellum/js/libpannellum.js";

/* LIGHTGALLERY */
// @codekit-prepend "/lightgallery/dist/js/lightgallery.js";
// @codekit-prepend "/lightgallery/modules/lg-thumbnail.js";



/* OWL CAROUSEL  */
// @codekit-prepend "/owlcarousel/dist/owl.carousel.min.js";

/* TWEENMAX */
// @codekit-prepend "/gsap/src/minified/TweenMax.min.js";

/* SCROLLMAGIC */
// @codekit-prepend "/scrollmagic/scrollmagic/minified/ScrollMagic.min.js";
// @codekit-prepend "/scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js";