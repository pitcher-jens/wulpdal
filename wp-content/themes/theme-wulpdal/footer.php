<?php 
$type = get_post_type( get_the_ID() ); 
if ($type == 'woningen') {
    $class = 'content-overlap';
}
?>

<footer class="dark-grey-bg <?php echo $class; ?>">
    
<?php
// NIEUWSBRIEF
$footer_news = get_field('footer_news');
if ($footer_news == 1 || $type == 'woningen') {
if( have_rows('footer_nieuwsbrief', 'option') ): 
while( have_rows('footer_nieuwsbrief', 'option') ): the_row(); 		
$titel = get_sub_field('titel');
$tekst = get_sub_field('tekst');
$formulier = get_sub_field('formulier');
?>
    
    <div class="nieuws">
        <div class="container">
            <div class="row center">
                <div class="col">
                    <div class="divider"></div>
                    <div class="title white">
                        <h2><span><?php echo $titel; ?></span></h2>
                        
                        <h3><?php echo $tekst; ?></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 form">
                    <div class=" dark" id="nieuwsbrief">
                        <?php echo do_shortcode( $formulier ); ?>
                    </div>
                </div>
                <div class="col-12 col-lg-8">
                    <div class="owl-carousel" id="nieuws-grid">
                        
                        <?php 
                        //LOAD PRODUCTEN
                        $args=array(
                        'post_type' => 'Nieuws',
                        'post_status' => 'publish',
                        'posts_per_page' => 2,
                        'orderby' => 'date',
                        'order' => 'DESC',
                        );
                        $my_query = null;
                        $my_query = new WP_Query($args);
                        if( $my_query->have_posts() ) {
                            while ($my_query->have_posts()) : $my_query->the_post();

                            $title = get_the_title();
                            $afbeelding = get_field('afbeelding');
                            $tekst = custom_field_excerpt();
                            $link = get_permalink();
                            
                            echo    '<div class="nieuws">';
                            echo    '<a href="' . $link . '">';
                            echo    '<div class="image" style="background-image: url(' . $afbeelding . ')"></div>';
                            echo    '<div class="content center">';
                            echo    '<h4>' . $title . '</h4>';
                            echo    $tekst;
                            echo    '<div class="button btn">Lees bericht</div>';
                            echo    '</div>';
                            echo    '</a>';
                            echo    '</div>';

                            endwhile;
                        } wp_reset_query();
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<?php  	
endwhile;
endif;
}
?>   
    
    <div class="contactgegevens light-grey-bg">
        <div class="container">
            <div class="row">
                
                <ul class="col-12 col-sm-6 col-md-6 col-lg-4 offset-md-0">
                
                <?php
                // FOOTER COL 1
                if( have_rows('footer_col_1', 'option') ): 
                    while( have_rows('footer_col_1', 'option') ): the_row(); 
                    
                        $titel = get_sub_field('titel');
                        echo    '<li>';
                        echo    '<h2>' . $titel . '</h2>';
                        echo    '</li>';

                            // REGELS
                            if( have_rows('regels') ): 
                                while( have_rows('regels') ): the_row(); 

                                    $tekst = get_sub_field('tekst');
                                    $link = get_sub_field('link');

                                    echo    '<li>';
                                    if ($link == '') { } else {
                                        echo    '<a href="' . $link[url] . '" target="' . $link[target] . '">';
                                    }
                                    echo    $tekst;
                                    if ($link == '') { } else {
                                        echo    '</a>';
                                    }
                                    echo    '</li>';
   
                                endwhile;
                            endif;  
                            // END REGELS


                    endwhile;
                endif;         
                ?>
                    
                <?php
                // FOOTER COL 3
                if( have_rows('footer_social', 'option') ): 
                    while( have_rows('footer_social', 'option') ): the_row(); 


                            // REGELS
                            if( have_rows('social_links') ): 
                                
                                echo    '<li class="social">';
                                echo    '<ul>';
                    
                                while( have_rows('social_links') ): the_row(); 

                                    $icoon = get_sub_field('icoon');
                                    $link = get_sub_field('link');
                                    
                                    echo    '<li><a href="' . $link[url] . '" target="' . $link[target] . '"><i class="fa fa-lg ' . $icoon . '" aria-hidden="true"></i></a></li>';
                    
                                endwhile;
                    
                                echo    '</ul>';
                                echo    '</li>';
                    
                            endif;  
                            // END REGELS


                    endwhile;
                endif;         
                ?>
                                        
                </ul>
                
                
                <?php
                // FOOTER COL 2
                if( have_rows('footer_col_2', 'option') ): 
                    while( have_rows('footer_col_2', 'option') ): the_row(); 


                            // REGELS
                            if( have_rows('regels') ): 
                
                                echo    '<ul class="col-12 col-sm-5 offset-sm-1 col-md-6 offset-md-0 col-lg-5 offset-lg-3 sitemap">';
                
                                while( have_rows('regels') ): the_row(); 

                                    $tekst = get_sub_field('tekst');
                                    $link = get_sub_field('link');
                
                                    echo    '<li><a href="' . $link[url] . '" class="link" target="' . $link[target] . '"> ' . $tekst . '</a></li>';
                
                                endwhile;
                
                                echo    '</ul>';      
                
                            endif;  
                            // END REGELS


                    endwhile;
                endif;         
                ?>
                
                
            </div>
        </div>
    </div>
</footer>

<?php
//LOAD HEADER SCRIPTS
if( have_rows('scripts', 'option') ): 
    while( have_rows('scripts', 'option') ): the_row(); 
        $locatie = get_sub_field('locatie');
        if ($locatie == 'Footer') {
            echo    get_sub_field('script');
        }
    endwhile;
endif;  
?>

<?php wp_footer(); ?>

</body>
</html>

<script>
    jQuery('.sfeerimpressie').on('click', function() {
        $(this).lightGallery({
            dynamic: true,
            thumbnail: true,
            dynamicEl: [{
                "src": 'https://lo-studio.nl/wp-content/upload_folders/lo-studio.nl/plattegrond-huis.jpg',
                'thumb': 'https://lo-studio.nl/wp-content/upload_folders/lo-studio.nl/plattegrond-huis.jpg'

            }, {
                "src": 'http://makelaarvooreenuur.nl/wp-content/uploads/2017/06/begane-grond_79984903.jpg',
                'thumb': 'http://makelaarvooreenuur.nl/wp-content/uploads/2017/06/begane-grond_79984903.jpg'
            }]
        })

    });
    
    /*
     * Replace all SVG images with inline SVG
     */
    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

});

</script>